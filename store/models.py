from django.db import models

# Create your models here.
# from django.db import models


class Artist(models.Model):
    name = models.CharField('nom',max_length=200,unique=True)

    class Meta:
        verbose_name = "artiste"

    def __str__(self):
        return self.name

class Album(models.Model):
    reference = models.IntegerField('référence',null=True)
    create_at = models.DateTimeField('date de création',auto_now_add=True)
    available = models.BooleanField('disponible',default=True)
    title = models.CharField('titre',max_length=200)
    picture = models.URLField('image')
    artists = models.ManyToManyField(Artist, related_name='albums', blank=True)

    class Meta:
        verbose_name = "disque"

    def __str__(self):
        return self.title



class Contact(models.Model):
    email = models.CharField('adresse mail',max_length=200)
    name = models.CharField('nom',max_length=200)

    class Meta :
        verbose_name = "prospect"

    def __str__(self):
        return self.name

class Booking(models.Model):
    create_at = models.DateTimeField('date de création',auto_now_add=True)
    contacted = models.BooleanField('reserver',default=False)
    album =models.OneToOneField(Album, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "réservation"

    def __str__(self):
        return self.contact.name

