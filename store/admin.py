from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse

from  .models import Booking, Contact, Album,Artist
from django.utils.safestring import mark_safe


class AdminURLMixin(object):
    def get_admin_url(self, obj):
        content_type = ContentType.objects.get_for_model(obj.__class__)
        return reverse("admin:store_%s_change" % (
            content_type.model),
            args=(obj.id,))

# un filtre :
@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin,AdminURLMixin):
    #list_filter = ['create_at', 'contacted']
    readonly_fields = ["create_at","contact_link","album_link","contacted"]

    def contact_link(self,booking):
        url = self.get_admin_url(booking.contact)
        return  mark_safe("<a href='{}'>{}</a>".format(url,booking.contact.name))

    def album_link(self,booking):
        url = self.get_admin_url(booking.album)
        return  mark_safe("<a href='{}'>{}</a>".format(url,booking.album.title))

    def has_add_permission(self, request):
        return False


# affichage des reservation

# affichage des reservation
class BookingInline(admin.TabularInline,AdminURLMixin):
    verbose_name = "Réservation"
    verbose_name_plural = "Réservations"
    readonly_fields = ["create_at","album_link","contacted"]
    model = Booking
    fieldsets = [
        (None,{'fields' : ['create_at',"album_link", 'contacted']})
    ]
    extra = 0

    def album_link(self,booking):
        url = self.get_admin_url(booking.album)
        return  mark_safe("<a href='{}'>{}</a>".format(url,booking.album.title))
    album_link.short_description = "Album"

    def has_add_permission(self, request, obj):
        return False


# affichage des contact/ prospect
@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    inlines = [BookingInline,]

class AlbumArtistInline(admin.TabularInline):
    verbose_name = "Disque"
    verbose_name_plural = "Disques"
    model = Album.artists.through
    extra = 1

# affichage des artistes
@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    inlines = [AlbumArtistInline]

# affichage des disques
@admin.register(Album)
class ArtistAdmin(admin.ModelAdmin):
    search_fields = ['reference', 'title']


