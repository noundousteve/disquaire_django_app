#from django.conf.urls import url
#form . import view

from store import views
from django.urls import path

urlpatterns = [
    path('', views.listing, name='listing'),
    path('<int:album_id>/', views.detail, name='detail'),
    path('search/', views.search, name='search')
]